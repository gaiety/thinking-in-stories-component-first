import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  Standard,
  Admin,
} from 'emberconf-2022/components/contact-card.stories';
import { renderStory } from '@storybook/ember-cli-storybook/test-support';

module('Integration | Component | contact-card', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders expected name', async function (assert) {
    await renderStory(Standard);

    assert.dom('[data-testid=cc-name]').includesText(Standard.args.name);
  });

  test('it renders an avatar', async function (assert) {
    await renderStory(Standard);

    assert
      .dom('[data-testid=cc-avatar]')
      .hasAttribute('src', Standard.args.avatar);
  });

  test('it is not an admin by default', async function (assert) {
    await renderStory(Standard);

    assert.dom('[data-testid=cc-admin]').doesNotExist();
  });

  test('it can be an admin', async function (assert) {
    await renderStory(Admin);

    assert.dom('[data-testid=cc-admin]').exists();
  });
});
