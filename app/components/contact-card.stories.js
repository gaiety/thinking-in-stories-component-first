import { hbs } from 'ember-cli-htmlbars';

export default {
  title: 'Contact Card',
  argTypes: {
    isAdmin: { control: 'boolean' },
  },
};

const Template = (args) => ({
  template: hbs`<ContactCard @name={{this.name}} @avatar={{this.avatar}} @isAdmin={{this.isAdmin}} />`,
  context: args, // context = args = this
});

export const Standard = Template.bind({});
Standard.args = {
  name: 'Zoey',
  avatar: 'https://svgshare.com/i/fVd.svg',
};

export const Admin = Template.bind({});
Admin.args = {
  ...Standard.args,
  isAdmin: true,
};
