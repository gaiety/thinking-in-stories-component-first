# Thinking in Stories: Component-First

This is an example Ember app that was the result of live-coding at EmberConf 2022 focusing on Storybook and tests. This code is _not_ intended to deploy in any meaningful capacity and is supplied merely as a reference and playground.

This readme explains how to get started with this project as well as lists all links referenced in the conference talk.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)

## Installation

* `git clone <repository-url>` this repository
* `cd emberconf-2022`
* `npm install`

## Running / Development

### Ember

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Storybook

* `npm run storybook`

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Linting

* `npm run lint`
* `npm run lint:fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

## Further Reading / Useful Links

* [Author notion doc for talk](https://hergaiety.notion.site/Thinking-in-Stories-Component-First-66c7c795e4864ed0a81fddbf777a6f86) (has abstract, outline, key snippets etc)
* [Author Portfolio](https://gaiety.me/)
* [Storybook - What's a Story](https://storybook.js.org/docs/react/get-started/whats-a-story)
* [Storybook - Install](https://storybook.js.org/docs/ember/get-started/install) (switch to "Ember")
* [Storybook configuration snippet referenced](https://gitlab.com/-/snippets/2270344) (or see it in this repo under `.storybook/main.js`)
* [TailwindCSS](https://tailwindcss.com/)
* [Tailwind UI Contact Card](https://tailwindui.com/components/application-ui/lists/grid-lists) (note, this is behind a paywall and assumes a Tailwind UI subscription)
* [Ember Template Imports](https://github.com/ember-template-imports/ember-template-imports)
* [Glint #ember-components](https://github.com/typed-ember/glint#ember-components)
* [Ember Blueprints](https://cli.emberjs.com/release/advanced-use/blueprints/)
* [Ember CLI Storybook](https://storybook.js.org/addons/@storybook/ember-cli-storybook)

### Standard Ember Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
